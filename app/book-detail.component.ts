import 'rxjs/add/operator/switchMap';
import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router'

import { Book } from './book';
import { BookService } from './book-service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})

export class BookDetailComponent  {
  @Input() book: Book;
  editName: string;
  editAuthor: string;

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private bookService: BookService )
    {}

    ngOnInit(): void {
        this.route.params
          .switchMap((params: Params) =>
          this.bookService.getBook(+params['id']))
          .subscribe((book: Book) => this.book = book);
      }

  goBack(): void {
    this.location.back();
  }
}
