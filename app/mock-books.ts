import { Book } from './book';

export const BOOKS: Book[] = [
  { id:1, author:'Redliński', name:'Telefrenia'  },
  { id:2, author:'Rylski', name:'Warunek'  },
  { id:3, author:'Hornby', name:'Długa droga w dół'  },
  { id:4, author:'Faber', name:'Szkarłatny płatek i biały'  },
  { id:5, author:'Oz', name:'Opowieść o miłości i mroku'  },
  { id:6, author:'Enquist', name:'Opowieść o Blanche i Marie'  },
  { id:7, author:'Arto', name:'Wyjący młynarz' },
  { id:8, author:'Myśliwski', name:'Traktat o łuskaniu fasoli'  },
  { id:9, author:'Lessing', name:'Pamiętnik przetrwania'  },
  { id:10 , author:'Grisham', name:'Niewinny'  }
];
