import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Book } from './book';

@Injectable()
export class BookService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private booksUrl: string = 'app/books.json';

  constructor(private http:Http) {}

  getBooks() {
  return this.http.get(this.booksUrl)
             .map((response: Response) => response.json());

  }

 getBook(id: number | string) {
   return this.http.get(this.booksUrl)
     .map((response: Response) => response.json())
     .toPromise()
     .then(books => books.find(book => book.id === +id));
   }

    remove(id: number | string)  {
      return this.http.delete(this.booksUrl, {headers: this.headers})
      .map((response: Response) => response.json())
      .toPromise()
      .then(() => null);
    }

  }
