import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Book } from './book';
import { BookService } from './book-service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books: Book[];
  selectedBook: Book;

  constructor(
    private router: Router,
    private bookService: BookService) {  }

  getBooks(): void {
    this.bookService.getBooks().subscribe(books => this.books = books);
  }

  ngOnInit(): void {
    this.getBooks();
  }

  gotoDetail(book: Book): void {
    this.router.navigate(['/detail', { id: book.id}]);
  }

  deleteBook(index) {
    this.books.splice(index, 1);
  }

private remove(item) {
  this.books = this.books.filter(book => book !== item);
}
}
